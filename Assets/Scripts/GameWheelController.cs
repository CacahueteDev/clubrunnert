using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWheelController : MonoBehaviour
{
    [SerializeField] private Vector3 axis;
    private float _speed;
    
    void Start()
    {
        SetSpeed(50);
    }

    void Update()
    {
        transform.Rotate(axis * (Time.deltaTime * _speed));
    }

    public void SetSpeed(float speed)
    {
        _speed = speed;
    }
}
