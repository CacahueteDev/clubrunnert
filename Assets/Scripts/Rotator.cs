using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] private Vector3 axis;
    
    void Start()
    {
        
    }

    void Update()
    {
        transform.Rotate(axis * Time.deltaTime);
    }
}
