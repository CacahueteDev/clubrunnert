using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WineBottleController : MonoBehaviour
{
    [SerializeField] private GameObject[] models;
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip[] clinkClips;
    private Vector3 _scale;
    private Vector3 _position;
    private Vector3 _rotation;
    
    public bool IsActive { get; private set; }
    
    void Start()
    {
        _scale = transform.localScale;
        _position = transform.position;
        _rotation = transform.eulerAngles;
    }

    void Update()
    {
        
    }

    public void Enter()
    {
        IsActive = false;
        
        foreach (GameObject model in models) 
            model.SetActive(false);
    }

    public void Leave()
    {
        if (Random.Range(0, 10) != 1) return;

        IsActive = true;
        
        foreach (GameObject model in models) 
            model.SetActive(true);

        /*
        transform.eulerAngles = _rotation + new Vector3(
            Random.Range(-45f, 45f),
            Random.Range(-45f, 45f),
            Random.Range(-45f, 45f)
        );*/
        transform.localScale = _scale * Random.Range(0.5f, 1.5f);
        
        source.PlayOneShot(clinkClips[Random.Range(0, clinkClips.Length)]);
    }
}
