using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button exitButton;
    
    void Start()
    {
        playButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene("Scenes/OutdoorsScene");
        });
        
        exitButton.onClick.AddListener(Application.Quit);
    }
}
