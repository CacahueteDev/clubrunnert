﻿using System;
using UnityEngine;

public class WineBottleZoneController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        WineBottleController wine = other.GetComponent<WineBottleController>();
        if (!wine) return;
        
        wine.Enter();
    }

    private void OnTriggerExit(Collider other)
    {
        WineBottleController wine = other.GetComponent<WineBottleController>();
        if (!wine) return;
        
        wine.Leave();
    }
}