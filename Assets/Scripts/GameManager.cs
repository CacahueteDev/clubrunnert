using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [SerializeField] private TextMeshProUGUI scoreText;
    
    public int Score { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        InvokeRepeating(nameof(IncrementScore), 1f, 0.25f);
    }

    void Update()
    {
        
    }

    public void IncrementScore(int amount = 1) => SetScore(Score + amount);

    public void SetScore(int score)
    {
        Score = score;
        scoreText.text = $"Score: {score}";
    }

    public void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
