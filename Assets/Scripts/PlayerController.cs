using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private float jumpPower;
    [SerializeField] private float gravityPower;
    [SerializeField] private LayerMask gravityLayer;
    [SerializeField] private float gravityDistance;
    
    public bool IsGrounded { get; private set; }
    
    void Start()
    {
        
    }

    void Update()
    {
        IsGrounded = Physics.Raycast(transform.position, Vector3.down, gravityDistance, gravityLayer);
        
        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded) Jump();
        
        rigidbody.AddForce(Vector3.down * gravityPower, ForceMode.Acceleration);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + Vector3.down * gravityDistance);
    }

    public void Jump()
    {
        rigidbody.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.gameObject.CompareTag("Obstacle")) return;

        WineBottleController bottleCtrl = other.gameObject.GetComponent<WineBottleController>();
        if (!bottleCtrl.IsActive) return;
        
        GameManager.Instance.ResetGame();
    }
}
