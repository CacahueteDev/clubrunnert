using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WineOceanController : MonoBehaviour
{
    [SerializeField] private Animator oceanAnimator;
    
    public bool IsRaised { get; private set; }
    
    void Start()
    {
        StartCoroutine(RaiseCoroutine());
    }

    void Update()
    {
        
    }

    IEnumerator RaiseCoroutine()
    {
        yield return new WaitForSeconds(Random.Range(2f, 15f));
        
        SetRaised(true);
        
        yield return new WaitForSeconds(Random.Range(5f, 20f));
        
        SetRaised(false);
    }

    public void SetRaised(bool raised)
    {
        IsRaised = raised;
        oceanAnimator.SetBool("raise", raised);
    }
}
